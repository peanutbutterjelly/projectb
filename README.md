# SWEN30006 Project B
Project B for SWEN30006 Software Modelling and Design at the University of Melbourne

---

Students (sorted according to student number)

 - Joanna Grace Cho Ern Lee (710094)
 - Shing Sheung Daniel Ip (723508)
 - Nelson Kai Miin Chen (743322)

---

Report is hosted on Google Drive, only group member has the access to the Google Docs

<https://docs.google.com/document/d/179NMglnoq5cBOw6nbVMv8zcS7bqlKtnJqaInpgcyktU/edit>

State machine diagram and modifed UML are hosted on draw.io and stored on Google Drive

State Machine Diagram: <https://www.draw.io/#G0B0gKhWk_F--XYzI5amQ5b3FYeFE>

Modifed UML: <https://www.draw.io/#G0B0gKhWk_F--XYkQwOGVFaHVCdGs>
