package com.unimelb.swen30006.metromadness.trains;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.lang.Math;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.tracks.Line;

/**
 * This is an abstract class for all Trains.
 */
public abstract class Train {
	
	// Logger
	private static Logger logger = LogManager.getLogger();
	// The state that a train can be in 
	public enum State {
		IN_STATION, READY_DEPART, ON_ROUTE, WAITING_ENTRY, FROM_DEPOT
	}

	// Constants
	public static final double REFERENCE_RADIUS = 2;
	public static final double WAITING_WIDTH = 2;
	public static final int MAX_TRIPS=4;
	public static final Color FORWARD_COLOUR = Color.ORANGE;
	public static final Color OCCUPIED_COLOUR = Color.DARK_GRAY;
	public static final Color BACKWARD_COLOUR = Color.VIOLET;
	public static final Color WAITING_COLOR = Color.RED;
	public static final float TRAIN_WIDTH=4;
	public static final float TRAIN_LENGTH = 6;
	public static final float TRAIN_SPEED=50f;
	
	// The train's name
	private String name;

	// The line that this is traveling on
	private Line trainLine;

	// Passenger Information
	private int passengerCapacity;
	private ArrayList<Passenger> passengers;
	private float departureTimer;
	
	// Station and track and position information
	//private Track track;
	private Point2D.Float pos;

	// Direction and direction
	private boolean forward;
	private State state;

	// State variables
	private int numTrips;
	private boolean disembarked;
	private State previousState = null;
	
	private boolean waitingLong = false;

	/**
	 * Abstract method for embarking a Passenger at a station.
	 * @param p Passenger object.
	 * @return true if embarked successfully.
	 */
	public abstract void embark(Passenger p) throws Exception;
	
	/**
	 * Abstract method to get the toggle key for visualising different views.
     * @return key as an integer.
     */
    abstract public int getToggleKey();
	
	/**
	 * Constructor for Train Class.
	 * @param trainLine The Line that this Train belongs to.
	 * @param start Starting Station of this Train.
	 * @param forward Flag of the Train's direction.
	 * @param name Train's name.
	 * @param passengerCapacity Number of Passengers the Train can transport.
	 */
	public Train(Line trainLine, Station start, boolean forward, String name, int passengerCapacity){
		this.trainLine = trainLine;
		this.trainLine.addTrain(name, start);
		this.state = State.FROM_DEPOT;
		this.forward = forward;
		this.passengerCapacity = passengerCapacity;
		this.passengers = new ArrayList<Passenger>();
		this.name = name;
	}

	/**
	 * Getter for name.
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter for forward.
	 * @return forward
	 */
	public boolean isForward() {
		return forward;
	}

	/**
	 * Adds Passenger to the list of Passengers on the Train.
	 * @param p Passenger to add to list.
	 * @return boolean check if Passenger added successfully.
	 */
	public boolean addPassenger(Passenger p) {
		if (passengers.size() < passengerCapacity && p.shouldBoard(this)) {
			this.passengers.add(p);
			return true;
		}
		return false;
	}

	/**
	 * Getter for trainLine.
	 * @return trainLine
	 */
	public Line getTrainLine() {
		return trainLine;
	}

	/**
	 * Setter for trainLine.
	 * @param trainLine
	 */
	public void setTrainLine(Line trainLine) {
		this.trainLine = trainLine;
	}

	/**
	 * Getter for pos.
	 * @return pos Position of the Train in (x,y) space.
	 */
	public Point2D.Float getPos() {
		return pos;
	}

	/**
	 * Setter for pos.
	 * @param pos Position of the Train in (x,y) space.
	 */
	public void setPos(Point2D.Float pos) {
		this.pos = pos;
	}

	/**
	 * Update method for the Train, changes its State when necessary.
	 * @param delta The time span between the current frame and the last frame in seconds.
	 */
	public void update(float delta){
		// Update all passengers
		for(Passenger p: this.passengers){
			p.update(delta);
		}
		boolean hasChanged = false;
		if(previousState == null || previousState != this.state){
			previousState = this.state;
			hasChanged = true;
		}
		
		Station currStation = null;
		try{
			currStation = this.trainLine.getCurrStation(this);
		}catch(Exception e){}
		// Update the state
		switch(this.state) {
		case FROM_DEPOT:
			if(hasChanged){
				logger.info(this.name+ " is travelling from the depot: "+currStation.getName()+" Station...");
			}
			
			// We have our station initialized we just need to retrieve the next track, enter the
			// current station officially and mark as in station
			try {
				if(currStation.canEnter(this.trainLine)){
					
					currStation.enter(this);
					this.pos = (Point2D.Float) currStation.getPosition().clone();
					this.state = State.IN_STATION;
					this.disembarked = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		case IN_STATION:
			if(hasChanged){
				logger.info(this.name+" is in "+currStation.getName()+" Station.");
			}
			
			// When in station we want to disembark passengers 
			// and wait 10 seconds for incoming passengers
			if(!this.disembarked){
				this.disembark();
				this.departureTimer = currStation.getDepartureTime(this);
				this.disembarked = true;
			} else {
				// Count down if departure timer. 
				if(this.departureTimer>0){
					this.departureTimer -= delta;
				} else {
					// We are ready to depart, find the next track and wait until we can enter 
					try {
						boolean endOfLine = this.trainLine.endOfLine(currStation);
						if(endOfLine){
							this.forward = !this.forward;
							this.numTrips++;
						}
						this.trainLine.moveToNextTrack(this, this.forward);
						this.state = State.READY_DEPART;
						break;
					} catch (Exception e){
						// Massive error.
						return;
					}
				}
			}
			break;
		case READY_DEPART:
			if(hasChanged){
				logger.info(this.name+ " is ready to depart for "+currStation.getName()+" Station!");
			}
			
			// When ready to depart, check that the track is clear and if
			// so, then occupy it if possible.
			if(this.trainLine.canEnterTrack(this, this.forward)){
				this.trainLine.moveToNextStation(this, this.forward);
				this.trainLine.enterTrack(this);
				this.state = State.ON_ROUTE;
			}
			break;
		case ON_ROUTE:
			if(hasChanged){
				logger.info(this.name+ " enroute to "+currStation.getName()+" Station!");
			}
			
			// Checkout if we have reached the new station
			if(this.pos.distance(currStation.getPosition()) < 10 ){
				this.state = State.WAITING_ENTRY;
			} else {
				move(delta);
			}
			break;
		case WAITING_ENTRY:
			if(hasChanged){
				logger.info(this.name+ " is awaiting entry "+currStation.getName()+" Station..!");
				waitingLong = false;
			}else{
				waitingLong = true;
			}
			
			// Waiting to enter, we need to check the station has room and if so
			// then we need to enter, otherwise we just wait
			try {
				if(currStation.canEnter(this.trainLine)){
					this.trainLine.leaveTrack(this);
					this.pos = (Point2D.Float) currStation.getPosition().clone();
					currStation.enter(this);
					this.state = State.IN_STATION;
					this.disembarked = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}

	}

	/**
	 * Method to move Train along Track until it reaches the next Station.
	 * @param delta The time span between the current frame and the last frame in seconds.
	 */
	public void move(float delta){
		Station currStation = null;
		try{
			currStation = this.trainLine.getCurrStation(this);
		}catch(Exception e){}
		// Work out where we're going
		float angle = angleAlongLine(this.pos.x,this.pos.y,currStation.getPosition().x,currStation.getPosition().y);
		float newX = this.pos.x + (float)( Math.cos(angle) * delta * TRAIN_SPEED);
		float newY = this.pos.y + (float)( Math.sin(angle) * delta * TRAIN_SPEED);
		this.pos.setLocation(newX, newY);
	}
	
	/**
	 * Allows Passengers to disembark at the current Station.
	 * @return disembarking ArrayList of Passengers that are disembarking.
	 */
	public ArrayList<Passenger> disembark(){
		Station currStation = null;
		try{
			currStation = this.trainLine.getCurrStation(this);
		}catch(Exception e){}
		ArrayList<Passenger> disembarking = new ArrayList<Passenger>();
		Iterator<Passenger> iterator = this.passengers.iterator();
		while(iterator.hasNext()){
			Passenger p = iterator.next();
			if(p.shouldLeave()){
				logger.info("Passenger "+p.getId()+" is disembarking at "+currStation.getName());
				disembarking.add(p);
				p.setRidingOn(null);
				iterator.remove();
			}
		}
		return disembarking;
	}

	/**
	 * Returns a string representation of this object.
     * @return String
     */
	@Override
	public String toString() {
		return "Train [line=" + this.trainLine.getName() +", departureTimer=" + departureTimer + ", pos=" + pos + ", forward=" + forward + ", state=" + state
				+ ", numTrips=" + numTrips + ", disembarked=" + disembarked + "]";
	}

	/**
	 * Checks if Train is currently in a Station.
	 * @return boolean
	 */
	public boolean inStation(){
		return (this.state == State.IN_STATION || this.state == State.READY_DEPART);
	}
	
	/**
	 * Calculates the angle of the Train along the Line.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 * @return angle
	 */
	public float angleAlongLine(float x1, float y1, float x2, float y2){	
		return (float) Math.atan2((y2-y1),(x2-x1));
	}

	/**
	 * Renders the Train while it is waiting.
	 * @param renderer
	 * @param radius
	 */
	public void renderWaiting(ShapeRenderer renderer, double radius){
		if(this.waitingLong && this.state == State.WAITING_ENTRY){
			Color col = WAITING_COLOR;
			renderer.setColor(col);
			renderer.circle(this.pos.x, this.pos.y, (float)(radius + WAITING_WIDTH));
		}
	}
	
	/**
	 * Renders the Train in the visualisation constantly.
	 * @param renderer
	 */
	public void render(ShapeRenderer renderer){
		if(!this.inStation()){
			// generate the outer circle
			Color col = this.forward ? FORWARD_COLOUR : BACKWARD_COLOUR;
			float radius = (float)(REFERENCE_RADIUS * Math.sqrt((double)this.passengerCapacity));
			
			// after calculated outer circle, try to render waiting indicator
			this.renderWaiting(renderer, radius);
			
			renderer.setColor(col);
			renderer.circle(this.pos.x, this.pos.y, radius);
			
			// generate inner circle
			renderer.setColor(OCCUPIED_COLOUR);
			float innerRadius = (float)(REFERENCE_RADIUS * Math.sqrt((double)passengers.size()));
			renderer.circle(this.pos.x, this.pos.y, innerRadius);
		}
	}
	
	/**
	 * Gets the current Station that the Train is in.
	 * @return current Station
	 */
	public Station currentStation(){
		Station curr = null;
		try{
			curr = this.trainLine.getCurrStation(this);
		}catch(Exception e){
			e.printStackTrace();
		}
		return curr;
	}
}
