package com.unimelb.swen30006.metromadness.trains;

import java.util.ArrayList;

import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.tracks.Line;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * This is a class for Cargo Trains, which extends the abstract class Train. These trains allow both 
 * Passengers with and without cargo to board and only stop at Cargo Stations.
 */
public class CargoTrain extends Train {

    // Train's cargo capacity
	private int availableCargo;
	private int cargoCapacity;
	
	// Constants for visualisation
	public static final double CARGO_REFERENCE_RADIUS = 0.5;
	public static final Color CARGO_FORWARD_COLOUR = Color.CYAN;
	public static final Color CARGO_BACKWARD_COLOUR = Color.LIME;

	private static final int toggleKey = Input.Keys.C;
	private static Double lastToggle = new Double(0);
	private static boolean showingPassenger = true;
	
	/**
	 * Constructor for CargoTrain class.
     * @param trainLine The Line that this Train belongs to.
     * @param start Starting Station of this Train.
     * @param forward Flag of the Train's direction.
     * @param name Train's name.
     * @param passengerCapacity Number of Passengers the Train can transport.
	 * @param cargoCapacity Amount of Cargo the Train can transport.
	 */
	public CargoTrain(Line trainLine, Station start, boolean forward, String name, int passengerCapacity, int cargoCapacity) {
		super(trainLine, start, forward, name, passengerCapacity);
		
		this.availableCargo = cargoCapacity;
		this.cargoCapacity = cargoCapacity;
	}

	/**
	 * Embarks a Passenger at a Station as well as all of their cargo if the available capacity is sufficient.
	 * @return true if the Passenger embarked successfully.
	 */
	@Override
	public void embark(Passenger p) throws Exception{
		int cargoWeight = p.getCargoWeight();
		if (cargoWeight <= availableCargo && addPassenger(p)) {
			p.setRidingOn(this);
			availableCargo -= cargoWeight;
			return;
		}
		throw new Exception();
	}
	
	/**
     * Disembarks Passengers at a Station as well as all of their cargo.
     * @return disembarking ArrayList of all the Passengers that disembarked.
     */
	@Override
	public ArrayList<Passenger> disembark(){
		ArrayList<Passenger> disembarking = super.disembark();
		for (Passenger p : disembarking) {
			availableCargo += p.getCargoWeight();
		}
		return disembarking;
	}
	
    /**
     * Renders the CargoTrain in visualisation constantly. Outer circle represents cargo capacity of the
     * Train while inner circle represents the current amount of cargo on the Train.
     * @param renderer
     */
	@Override
	public void render(ShapeRenderer renderer){
		if(showingPassenger){
			super.render(renderer);
		}else{
			if(!this.inStation()){
				// generate the outer circle
				Color col = this.isForward() ? CARGO_FORWARD_COLOUR : CARGO_BACKWARD_COLOUR;
				float radius = (float)(CARGO_REFERENCE_RADIUS * Math.sqrt((double)this.cargoCapacity));
				
				// after calculated outer circle, try to render waiting indicator
				this.renderWaiting(renderer, radius);
				
				renderer.setColor(col);
				renderer.circle(this.getPos().x, this.getPos().y, radius);
				
				// generate inner circle
				renderer.setColor(OCCUPIED_COLOUR);
				float innerRadius = (float)(CARGO_REFERENCE_RADIUS * Math.sqrt((double)cargoCapacity - availableCargo));
				renderer.circle(this.getPos().x, this.getPos().y, innerRadius);
			}
		}
	}
	
	/**
	 * Toggles visualisation of the current cargo onboard to the current number of Passengers onboard.
	 */
	public static void toggleVisualisation(){
		if(CargoTrain.lastToggle >= 1){
			CargoTrain.showingPassenger = !CargoTrain.showingPassenger;
			CargoTrain.lastToggle = new Double(0);
		}
	}
	
	/**
	 * Ensures that the toggle is triggered more than a second apart.
	 * @param delta
	 */
	public static void countToggle(Double delta){
		CargoTrain.lastToggle += delta;
	}
	
    /**
     * Gets the toggle key for visualising different views.
     * @return key as an integer.
     */
	@Override
	public int getToggleKey(){
		return CargoTrain.toggleKey;
	}
}
