package com.unimelb.swen30006.metromadness.trains;

import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.tracks.Line;

/**
 * This is a class for Passenger Trains, which extends the abstract class Train. These trains only allows 
 * Passengers without cargo to board.
 */
public class PassengerTrain extends Train {

	private static final int toggleKey = 0;
	
	/**
     * Constructor for PassengerTrain class.
     * @param trainLine The Line that this Train belongs to.
     * @param start Starting Station of this Train.
     * @param forward Flag of the Train's direction.
     * @param name Train's name.
     * @param passengerCapacity Number of Passengers the Train can transport.
     */
	public PassengerTrain(Line trainLine, Station start, boolean forward, String name, int passengerCapacity) {
		super(trainLine, start, forward, name, passengerCapacity);
	}

    /**
     * Embarks a Passenger at a Station as well as all of their cargo if the available capacity is sufficient.
     * @return true if the Passenger embarked successfully.
     */
	@Override
	public void embark(Passenger p) throws Exception{
		if(addPassenger(p)){
			p.setRidingOn(this);
			return;
		}
		throw new Exception();
	}

    /**
     * Gets the toggle key for visualising different views.
     * @return key as an integer.
     */
	@Override
	public int getToggleKey(){
		return PassengerTrain.toggleKey;
	}
	
}
