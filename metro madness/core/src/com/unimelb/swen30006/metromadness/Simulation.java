package com.unimelb.swen30006.metromadness;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.tracks.Line;
import com.unimelb.swen30006.metromadness.trains.Train;

public class Simulation {
	
	public ArrayList<Station> stations;
	public ArrayList<Line> lines;
	public ArrayList<Train> trains;
	
	private HashMap<Integer, HashSet<Class<?>>> visualisationToggleHandler;
	
	public Simulation(String fileName){
		// Create a map reader and read in the file
		MapReader m = new MapReader(fileName);
		m.process();
		
		// Create a list of lines
		this.lines = new ArrayList<Line>();
		this.lines.addAll(m.getLines());
				
		// Create a list of stations
		this.stations = new ArrayList<Station>();
		this.stations.addAll(m.getStations());
		
		// Create a list of trains
		this.trains = new ArrayList<Train>();
		this.trains.addAll(m.getTrains());
		
		HashMap<Integer, HashSet<Class<?>>> handler = new HashMap<Integer, HashSet<Class<?>>>();
		for(Train train : trains){
			if(train.getToggleKey() != 0){
				if(!handler.containsKey(train.getToggleKey())){
					HashSet<Class<?>> trainsClasses = new HashSet<Class<?>>();
					handler.put(train.getToggleKey(), trainsClasses);
				}
				HashSet<Class<?>> trainsClasses = handler.get(train.getToggleKey());
				trainsClasses.add(train.getClass());
			}
		}
		for(Station station : stations){
			if(station.getToggleKey() != 0){
				if(!handler.containsKey(station.getToggleKey())){
					HashSet<Class<?>> trainsClasses = new HashSet<Class<?>>();
					handler.put(station.getToggleKey(), trainsClasses);
				}
				HashSet<Class<?>> stationsClasses = handler.get(station.getToggleKey());
				stationsClasses.add(station.getClass());
			}
		}
		this.visualisationToggleHandler = handler;
	}
	
	public HashMap<Integer, HashSet<Class<?>>> getVisualisationToggleHandler(){
		return visualisationToggleHandler;
	}
	
	
	// Update all the trains in the simulation
	public void update(){
		// Update all the trains
		for(Train t: this.trains){
			t.update(Gdx.graphics.getDeltaTime());
		}
		
		// This part is for generic Train toggling time counter
		for(Integer key : visualisationToggleHandler.keySet()){
        	for(Class<?> c : visualisationToggleHandler.get(key)){
        		try{
        			Method m = c.getMethod("countToggle", Double.class);
        			m.invoke(null, new Double(Gdx.graphics.getDeltaTime()));
        		}catch(Exception e){
        		}
        	}
        }
	}
	
	public void render(ShapeRenderer renderer){
		for(Line l: this.lines){
			l.render(renderer);
		}

		for(Train t: this.trains){
			t.render(renderer);
		}
		for(Station s: this.stations){
			s.render(renderer);
		}
	}
	
}
