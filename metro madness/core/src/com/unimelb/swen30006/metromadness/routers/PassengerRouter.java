package com.unimelb.swen30006.metromadness.routers;

import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is an interface for all PassengerRouter, the passenger will be routed by routers.
 */
public interface PassengerRouter {

	/**
	 * Should the passenger leave the station
	 * @param current current station of the passenger is located at
	 * @param p The passenger
	 * @return true if the passenger should leave at the station
	 */
	public boolean shouldLeave(Station current, Passenger p);
	
	/**
	 * Should the passenger board the train
	 * @param train The train that the passenger is considering
	 * @param p The passenger
	 * @return true if the passenger should board this train
	 */
	public boolean shouldBoard(Train train, Passenger p);
}
