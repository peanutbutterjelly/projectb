package com.unimelb.swen30006.metromadness.routers;

import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.trains.Train;

public class SimpleRouter implements PassengerRouter {
	
	private static SimpleRouter instance = new SimpleRouter();

	/**
	 * All Simple passenger use the same router, therefore, it is a singleton pattern
	 * @return The router singleton
	 */
	public static SimpleRouter getInstance(){
		return instance;
	}
	
	@Override
	public boolean shouldLeave(Station current, Passenger p) {
		return current.equals(p.getDestination());
	}

	@Override
	public boolean shouldBoard(Train train, Passenger p){
		return train.getTrainLine().getStations().contains(p.getDestination());
	}
}
