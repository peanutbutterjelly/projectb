package com.unimelb.swen30006.metromadness.stations;

import java.util.ArrayList;
import java.util.Iterator;
import java.lang.Math;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.passengers.CargoPassengerGenerator;
import com.unimelb.swen30006.metromadness.tracks.Line;
import com.unimelb.swen30006.metromadness.trains.Train;

public class CargoStation extends Station {
	// Logger
	private static Logger logger = LogManager.getLogger();
	
	public static final double RADIUS_REFERENCE = 0.5;
	public static Double lastToggle = new Double(0);
	public static boolean showingTrain = true;
	
	public CargoPassengerGenerator g;
	public ArrayList<Passenger> waiting;
	public int maxVolume;
	
	private static final int toggleKey = Input.Keys.X;
	
	/**
	 * Constructor for PassiveStation Class.
	 * @param x x coordinate of the location of the station
	 * @param y y coordinate of the location of the station
	 * @param name Name of the station
	 * @param platforms Number of platforms
	 * @param departureTime Departure time for train to depart after enter
	 * @param maxPax The maximum number of passenger that the station can holds
	 */
	public CargoStation(float x, float y, String name, int platforms, int departureTime, int maxPax) {
		super(x, y, name, platforms, departureTime);
		this.waiting = new ArrayList<Passenger>();
		this.g = new CargoPassengerGenerator(this, this.getLines(), 4);
		this.maxVolume = maxPax;
	}
	
	@Override
	public void enter(Train t) throws Exception {
		if(getTrains().size() >= platforms){
			throw new Exception();
		} else {
			// Add the train
			this.getTrains().add(t);
			// Add the waiting passengers
			Iterator<Passenger> pIter = this.waiting.iterator();
			while(pIter.hasNext()){
				Passenger p = pIter.next();
				try {
					logger.info("Passenger "+p.getId()+" carrying "+p.getCargoWeight()+" kg cargo embarking at "+this.getName()+" heading to "+p.getDestination().getName());
					t.embark(p);
					pIter.remove();
				} catch (Exception e){
					// Do nothing, already waiting
					break;
				}
			}
			
			//Do not add new passengers if there are too many already
			if (this.waiting.size() >= maxVolume){
				return;
			}
			// Add the new passenger
			Passenger[] ps = this.g.generatePassengers();
			for(Passenger p: ps){
				if(this.waiting.size() >= this.maxVolume)
					break;
				try {
					logger.info("Passenger "+p.getId()+" carrying "+p.getCargoWeight()+" kg embarking at "+this.getName()+" heading to "+p.getDestination().getName());
					t.embark(p);
				} catch(Exception e){
					this.waiting.add(p);
				}
			}
		}
	}
	
	@Override
	public void render(ShapeRenderer renderer){
		float radius = this.getRadius();
		for(int i=0; (i<this.getLines().size() && i<MAX_LINES); i++){
			Line l = this.getLines().get(i);
			renderer.setColor(l.getColour());
			renderer.circle(this.getPosition().x, this.getPosition().y, radius, NUM_CIRCLE_STATMENTS);
			radius = radius - 1;
		}
		
		// Calculate the percentage
		float t;
		Color c;
		if(showingTrain){
			t = this.getTrains().size()/(float)platforms;
			c = Color.WHITE.cpy().lerp(Color.DARK_GRAY, t);
		}else{
			t = this.waiting.size()/(float)this.maxVolume;
			c = Color.WHITE.cpy().lerp(Color.RED, t);
		}
		
		renderer.setColor(c);
		renderer.circle(this.getPosition().x, this.getPosition().y, radius, NUM_CIRCLE_STATMENTS);		
	}

	@Override
	public float getRadius(){
		return (float)(RADIUS_REFERENCE * Math.sqrt(this.maxVolume));
	}
	
	/**
	 * Toggles visualisation of the current cargo onboard to the current number of Passengers onboard.
	 */
	public static void toggleVisualisation(){
		if(CargoStation.lastToggle >= 1){
			CargoStation.showingTrain = !CargoStation.showingTrain;
			CargoStation.lastToggle = new Double(0);
		}
	}
	
	/**
	 * Ensures that the toggle is triggered more than a second apart.
	 * @param delta
	 */
	public static void countToggle(Double delta){
		CargoStation.lastToggle += delta;
	}
	
	@Override
	public int getToggleKey(){
		return toggleKey;
	}
}
