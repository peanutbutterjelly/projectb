package com.unimelb.swen30006.metromadness.stations;

import java.awt.geom.Point2D;
import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.tracks.Line;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is an abstract class for all Stations.
 */
public abstract class Station {
	
	// attributes of the stations, moved away from static final by a lot
	// as we are looking for more flexible station for more platforms,
	// and maybe other departureTime,
	// so that we can run simulation even more ways and see how they work
	public int platforms;
	public static final int NUM_CIRCLE_STATMENTS=100;
	public static final int MAX_LINES=3;
	private Point2D.Float position;
	private float radius=6;
	private String name;
	private ArrayList<Line> lines;
	private ArrayList<Train> trains;
	private float departureTime;

	/**
	 * Constructor for Station Class.
	 * @param x x coordinate of the location of the station
	 * @param y y coordinate of the location of the station
	 * @param name Name of the station
	 * @param platforms Number of platforms
	 * @param departureTime Departure time for train to depart after enter
	 */
	public Station(float x, float y, String name, int platforms, int departureTime){
		this.name = name;
		this.position = new Point2D.Float(x,y);
		this.lines = new ArrayList<Line>();
		this.trains = new ArrayList<Train>();
		this.platforms = platforms;
		this.departureTime = departureTime;
	}
	
	/**
	 * Getter for number of platforms
	 * @return number of platforms
	 */
	public int getPlatforms() {
		return platforms;
	}

	/**
	 * Getter for the location of the station position
	 * @return the location of the station position
	 */
	public Point2D.Float getPosition() {
		return position;
	}

	/**
	 * Getter for radius rendered on map
	 * @return the radius rendered on map
	 */
	public float getRadius() {
		return radius;
	}

	/**
	 * Getter for the name of the station
	 * @return the name of the station
	 */
	public String getName() {
		return name;
	}

	/**
	 * Abstract method to get the toggle key for visualising different views.
     * @return key as an integer.
     */
    abstract public int getToggleKey();
	
	/**
	 * Getter for all the lines using this station
	 * @return all the lines using this station
	 */
	public ArrayList<Line> getLines() {
		return lines;
	}

	/**
	 * Getter for the trains currently in this station
	 * @return the trains currently in this station
	 */
	public ArrayList<Train> getTrains() {
		return trains;
	}
	
	/**
	 * Method for adding line to station
	 * @param l
	 */
	public void registerLine(Line l) throws Exception{
		if(this.lines.size() < MAX_LINES)
			this.lines.add(l);
		else
			throw new Exception();
	}
	
	/**
	 * Renders the Station in the visualisation constantly.
	 * @param renderer the gdx ShapeRenderer
	 */
	public void render(ShapeRenderer renderer){
		float radius = this.getRadius();
		for(int i=0; (i<this.lines.size() && i<MAX_LINES); i++){
			Line l = this.lines.get(i);
			renderer.setColor(l.getColour());
			renderer.circle(this.position.x, this.position.y, radius, NUM_CIRCLE_STATMENTS);
			radius = radius - 1;
		}
		
		// Calculate the percentage
		float t = this.trains.size()/(float)platforms;
		Color c = Color.WHITE.cpy().lerp(Color.DARK_GRAY, t);
		renderer.setColor(c);
		renderer.circle(this.position.x, this.position.y, radius, NUM_CIRCLE_STATMENTS);		
	}
	
	/**
	 * Train enter this station
	 * @param t the train entering
	 * @throws Exception All Platforms are occupied
	 */
	public void enter(Train t) throws Exception {
		if(trains.size() >= platforms){
			throw new Exception();
		} else {
			this.trains.add(t);
		}
	}
	
	/**
	 * Train depart from this station
	 * @param t the train departing
	 * @throws Exception The train is not in this station
	 */
	public void depart(Train t) throws Exception {
		if(this.trains.contains(t)){
			this.trains.remove(t);
		} else {
			throw new Exception();
		}
	}
	
	/**
	 * Check if there is available platforms
	 * @param l the line that the train is servicing
	 * @return true if there is at least 1 available platforms
	 */
	public boolean canEnter(Line l) {
		return trains.size() < platforms;
	}

	/**
	 * Getter for the departure time of this station
	 * @param train
	 * @return departure time in seconds
	 */
	public float getDepartureTime(Train train) {
		return departureTime;
	}

	@Override
	public String toString() {
		return "Station [position=" + position + ", name=" + name + ", trains=" + trains.size()
				+ "]";
	}
	
}
