package com.unimelb.swen30006.metromadness.stations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.tracks.Line;
import com.unimelb.swen30006.metromadness.trains.CargoTrain;
import com.unimelb.swen30006.metromadness.trains.Train;

public class PassiveStation extends Station {
	// Logger
	private static Logger logger = LogManager.getLogger();
	
	private static final int toggleKey = 0;
	
	/**
	 * Constructor for PassiveStation Class.
	 * @param x x coordinate of the location of the station
	 * @param y y coordinate of the location of the station
	 * @param name Name of the station
	 * @param platforms Number of platforms
	 * @param departureTime Departure time for train to depart after enter
	 */
	public PassiveStation(float x, float y, String name, int platforms, int departureTime) {
		super(x, y, name, platforms, departureTime);
	}
	
	@Override
	public void enter(Train t) throws Exception {
		if(getTrains().size() >= platforms){
			throw new Exception();
		} else {
			// Add the train
			this.getTrains().add(t);
		}
	}
	
	@Override
	public void render(ShapeRenderer renderer){
		float radius = this.getRadius();
		for(int i=0; (i<this.getLines().size() && i<MAX_LINES); i++){
			Line l = this.getLines().get(i);
			renderer.setColor(l.getColour());
			renderer.circle(this.getPosition().x, this.getPosition().y, radius, NUM_CIRCLE_STATMENTS);
			radius = radius - 1;
		}
		
		// Calculate the percentage
		float t = this.getTrains().size()/(float)platforms;
		Color c = Color.WHITE.cpy().lerp(Color.DARK_GRAY, t);
		
		renderer.setColor(c);
		renderer.circle(this.getPosition().x, this.getPosition().y, radius, NUM_CIRCLE_STATMENTS);		
	}

	@Override
	public float getDepartureTime(Train train) {
		if(train instanceof CargoTrain){
			return 0;
		}
		return super.getDepartureTime(train);
	}
	
	@Override
	public int getToggleKey(){
		return toggleKey;
	}
}
