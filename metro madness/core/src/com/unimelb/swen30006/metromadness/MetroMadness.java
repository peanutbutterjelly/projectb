package com.unimelb.swen30006.metromadness;

import java.util.HashMap;
import java.util.HashSet;
import java.lang.reflect.Method;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class MetroMadness extends ApplicationAdapter {

	// The width of the world in unitless dimensions
    public static final int WORLD_WIDTH = 1200;
    public static final int WORLD_HEIGHT = 1200;

    // Viewport state
    public int VIEWPORT_WIDTH=200;
    public float viewport_width;

	// Data for simluation, rendering and camera.
    public Simulation sim;
    public ShapeRenderer shapeRenderer;
    public OrthographicCamera camera;
	
	// Font
    public BitmapFont smaller;
    public BitmapFont header;
	
	// train visualisation toggling
    public HashMap<Integer, HashSet<Class<?>>> trainVisualisationToggleHandler;

	@Override
	public void resize(int width, int height) {
        camera.viewportWidth = viewport_width;
        camera.viewportHeight = viewport_width * (float)height/width;
        camera.update();
	}

	@Override
	public void create () {
		// Create the simulation
		sim = new Simulation("filename");		
		
		trainVisualisationToggleHandler = sim.getVisualisationToggleHandler();
		
		// Setup our 2D Camera
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        viewport_width = VIEWPORT_WIDTH;
		camera = new OrthographicCamera(viewport_width, viewport_width * (h / w));
		camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
		camera.update();
		
		// Create our shape renderer
		shapeRenderer = new ShapeRenderer();
		
		// Create our font
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("assets/fonts/Gotham-Book.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 28;
		smaller = generator.generateFont(parameter); // font size 12 pixels
		generator.dispose(); // don't forget to dispose to avoid memory leaks!

		FreeTypeFontGenerator headlineGen = new FreeTypeFontGenerator(Gdx.files.internal("assets/fonts/Gotham-Bold.ttf"));
		FreeTypeFontParameter headlineParam = new FreeTypeFontParameter();
		headlineParam.size = 40;
		header = headlineGen.generateFont(headlineParam); // font size 40 pixels
		headlineGen.dispose(); // don't forget to dispose to avoid memory leaks!
		
		// Setup fonts
		 smaller.setColor(Color.GRAY);
		 header.setColor(Color.BLACK);

	}

	@Override
	public void render () {
		// Clear the graphics to white
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Handle user input
		handleInput();
		
		// Update the simulation and camera
		camera.update();
		sim.update();
		
		// Render the simulation
		 shapeRenderer.setProjectionMatrix(camera.combined);
		 
		 // Render all filled shapes.
		 shapeRenderer.begin(ShapeType.Filled);
		 sim.render(shapeRenderer);
		 shapeRenderer.end();
		 
		 // Begin preparations to render text
		 SpriteBatch batch = new SpriteBatch();
		 batch.begin();

		 // Render Header
		 header.getData().setScale(0.5f);
		 header.draw(batch, "metro madness.", 10, Gdx.graphics.getHeight()-10);
		 
		 // Render Smaller
		 smaller.getData().setScale(0.5f);
		 smaller.draw(batch, "Press C to toggle Cargo Loading", 10, Gdx.graphics.getHeight()-38);
		 smaller.draw(batch, "Press Q/E to Zoom In/Out", 10, Gdx.graphics.getHeight()-58);
		 smaller.draw(batch, "Press WASD/arrow keys to move", 250, Gdx.graphics.getHeight()-38);
		 smaller.draw(batch, "Press Escape key to exit", 250, Gdx.graphics.getHeight()-58);
		 smaller.draw(batch, "Press X to toggle Station Train/Passenger Loading", 10, Gdx.graphics.getHeight()-78);
		 
		 batch.end();

	}
	
    private void handleInput() {
        if (Gdx.input.isKeyPressed(Input.Keys.E)) {
            camera.zoom += 0.1;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Q)) {
        	camera.zoom -= 0.1;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.A)){
        	camera.translate(-3f, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            Gdx.app.exit();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.D)) {
        	camera.translate(3f, 0, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.S)) {
        	camera.translate(0, -3f, 0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.W)) {
        	camera.translate(0, 3f, 0);
        }

        // This part is for generic Train render toggling, registered all the keys in a map
        for(Integer key : trainVisualisationToggleHandler.keySet()){
        	if(Gdx.input.isKeyPressed(key)){
	        	for(Class<?> c : trainVisualisationToggleHandler.get(key)){
	        		try{
	        			Method m = c.getMethod("toggleVisualisation");
	        			m.invoke(c);
	        		}catch(Exception e){
	        		}
	        	}
        	}
        }
        
        camera.zoom = MathUtils.clamp(camera.zoom, 0.1f, WORLD_WIDTH/camera.viewportWidth);
        float effectiveViewportWidth = camera.viewportWidth * camera.zoom;
        float effectiveViewportHeight = camera.viewportHeight * camera.zoom;

        camera.position.x = MathUtils.clamp(camera.position.x, effectiveViewportWidth / 2f, WORLD_WIDTH - effectiveViewportWidth / 2f);
        camera.position.y = MathUtils.clamp(camera.position.y, effectiveViewportHeight / 2f, WORLD_HEIGHT - effectiveViewportHeight / 2f);
    }

}
