package com.unimelb.swen30006.metromadness.tracks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is a Class for Line, which represents a train line with specified stops.
 */
public class Line {
	
	// The colour of this line
	private Color lineColour;
	private Color trackColour;
	
	// The name of this line
	private String name;
	// The stations on this line
	private ArrayList<Station> stations;
	// The tracks on this line between stations
	private ArrayList<Track> tracks;

	// map between train and station, train and track
	private HashMap<String, Station> trainAtStation = new HashMap<String, Station>();
	private HashMap<String, Track> trainAtTrack = new HashMap<String, Track>();
		
	/**
	 * Constructor for Line Class.
	 * @param stationColour colour of the Stations
	 * @param lineColour colour of the Line
	 * @param name of the Line
	 */
	public Line(Color stationColour, Color lineColour, String name){
		// Set the line colour
		this.lineColour = stationColour;
		this.trackColour = lineColour;
		this.name = name;
		
		// Create the data structures
		this.stations = new ArrayList<Station>();
		this.tracks = new ArrayList<Track>();
	}
	
	/**
	 * Getter for colour of Line.
	 * @return lineColour
	 */
	public Color getColour() {
		return lineColour;
	}
	
	/**
	 * Getter for name of Line.
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Getter for nth Station of Line.
	 * @param n
	 * @return nth Station
	 */
	public Station getNthStation(int n){
		return this.stations.get(n);
	}
	
	/**
	 * Getter for length of Line.
	 * @return Line length
	 */
	public int getLineLen(){
		return this.stations.size();
	}
	
	/**
	 * Gets index of specified Station on Line.
	 * @param station
	 * @return index
	 */
	public int getStationIndex(Station station){
		return this.stations.indexOf(station);
	}
	
	/**
	 * Checks if specified Station is the start of the Line.
	 * @param station
	 * @return boolean
	 */
	public boolean isStartOfLine(Station station){
		return this.getStationIndex(station) == 0;
	}
	
    /**
     * Checks if specified Station is the end of the Line.
     * @param station
     * @return boolean
     */
	public boolean isEndOfLine(Station station){
		return this.getStationIndex(station) == this.stations.size() - 1;
	}
	
	/**
	 * Adds a Train at specified Station.
	 * @param trainName
	 * @param station
	 */
	public void addTrain(String trainName, Station station){
		this.trainAtStation.put(trainName, station);
	}
	
	/**
	 * Adds a Station at the end of the Line.
	 * @param s Station
	 * @param two_way check if the track should be dual or mono.
	 */
	public void addStation(Station s, Boolean two_way){
		// We need to build the track if this is adding to existing stations
		if(this.stations.size() > 0){
			// Get the last station
			Station last = this.stations.get(this.stations.size()-1);
			
			// Generate a new track
			Track t;
			if(two_way){
				t = new DualTrack(last.getPosition(), s.getPosition(), this.trackColour);
			} else {
				t = new MonoTrack(last.getPosition(), s.getPosition(), this.trackColour);
			}
			this.tracks.add(t);
		}
		
		// Add the station
		try{
			s.registerLine(this);
		}catch(Exception e){
			System.out.println("Station reach the number of lines limit");
		}
		this.stations.add(s);
	}
	
	/**
	 * Getter for list of Stations on the Line.
	 * @return stations
	 */
	public ArrayList<Station> getStations(){
		return this.stations;
	}
	
    /**
     * Returns a string representation of this object.
     * @return String
     */
	@Override
	public String toString() {
		return "Line [lineColour=" + lineColour + ", trackColour=" + trackColour + ", name=" + name + "]";
	}

	/**
	 * Checks if reached the end of the Line.
	 * @param s currentStation
	 * @return boolean
	 * @throws Exception if empty Line with no Stations
	 */
	public boolean endOfLine(Station s) throws Exception{
		if(this.stations.contains(s)){
			int index = this.stations.indexOf(s);
			return (index==0 || index==this.stations.size()-1);
		} else {
			throw new Exception();
		}
	}

	/**
	 * Gets the next Track on the Line.
	 * @param train Train
	 * @param forward direction of Train
	 * @return Track
	 * @throws Exception if no Tracks found.
	 */
	public Track nextTrack(Train train, boolean forward) throws Exception {
		Station currentStation = this.trainAtStation.get(train.getName());
		if(this.stations.contains(currentStation)){
			// Determine the track index
			int curIndex = this.stations.lastIndexOf(currentStation);
			// Increment to retrieve
			if(!forward){ curIndex -=1;}
			
			// Check index is within range
			if((curIndex < 0) || (curIndex > this.tracks.size()-1)){
				throw new Exception();
			} else {
				return this.tracks.get(curIndex);
			}
			
		} else {
			throw new Exception();
		}
	}
	
    /**
     * Gets the next Station on the Line.
     * @param train Train
     * @param forward direction of Train
     * @return Station
     * @throws Exception if no Stations found.
     */
	public Station nextStation(Train train, boolean forward) throws Exception{
		Station s = this.trainAtStation.get(train.getName());
		if(this.stations.contains(s)){
			int curIndex = this.stations.lastIndexOf(s);
			if(forward){ curIndex+=1;}else{ curIndex -=1;}
			
			// Check index is within range
			if((curIndex < 0) || (curIndex > this.stations.size()-1)){
				throw new Exception();
			} else {
				return this.stations.get(curIndex);
			}
		} else {
			throw new Exception();
		}
	}
	
	/**
	 * Updates current Station that the Train is at
	 * @param train
	 * @param station
	 * @throws Exception if Train not found in Station.
	 */
	public void updateTrainStation(Train train, Station station) throws Exception{
		if(this.trainAtStation.containsKey(train.getName()) && 
				this.stations.contains(station)){
			this.trainAtStation.put(train.getName(), station);
		}else{
			throw new Exception();
		}
	}
	
    /**
     * Updates current Track that the Train is at
     * @param train
     * @param track
     * @throws Exception if Train not found in Track.
     */
	public void updateTrainTrack(Train train, Track track) throws Exception{
		if(this.trainAtStation.containsKey(train.getName()) && 
				this.tracks.contains(track)){
			this.trainAtTrack.put(train.getName(), track);
		}else{
			throw new Exception();
		}
	}
	
	/**
	 * Gets current Station the Train is at.
	 * @param train
	 * @return Station
	 * @throws Exception Train not in Line.
	 */
	public Station getCurrStation(Train train) throws Exception{
		if(this.trainAtStation.containsKey(train.getName())){
			return this.trainAtStation.get(train.getName());
		}else{
			throw new Exception();
		}
	}
	
	/**
	 * Moves Train to the next Station on the Line.
	 * @param train
	 * @param forward
	 */
	public void moveToNextStation(Train train, boolean forward){
		Station curr = null, next = null;
		try{
			next = this.nextStation(train, forward);
		}catch(Exception e){
			e.printStackTrace();
		}
		curr = this.trainAtStation.get(train.getName());
		try {
			curr.depart(train);
			this.updateTrainStation(train, next);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Moves Train to the next Track on the Line.
	 * @param train
	 * @param forward
	 */
	public void moveToNextTrack(Train train, boolean forward){
		Track next = null;
		try{
			next = this.nextTrack(train, forward);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			this.updateTrainTrack(train, next);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks if Train can enter Track.
	 * @param train
	 * @param forward
	 * @return boolean
	 */
	public boolean canEnterTrack(Train train, boolean forward){
		Track track = this.trainAtTrack.get(train.getName());
		return track.canEnter(forward);
	}
	
	/**
	 * Move Train into Track.
	 * @param train
	 */
	public void enterTrack(Train train){
		Track track = this.trainAtTrack.get(train.getName());
		track.enter(train);
	}
	
	/**
	 * Move Train out of Track.
	 * @param train
	 */
	public void leaveTrack(Train train){
		Track track = this.trainAtTrack.get(train.getName());
		track.leave(train);
	}
	
	/**
	 * Constantly render Line.
	 * @param renderer
	 */
	public void render(ShapeRenderer renderer){
		// Set the color to our line
		renderer.setColor(trackColour);
	
		// Draw all the track sections
		for(Track t: this.tracks){
			t.render(renderer);
		}	
	}
	
}
