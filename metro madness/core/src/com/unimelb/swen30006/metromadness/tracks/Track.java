package com.unimelb.swen30006.metromadness.tracks;

import java.awt.geom.Point2D;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is an abstract class for all Tracks.
 */
public abstract class Track {
    
    // Constants and variables for visualisation.
	public static final float DRAW_RADIUS=10f;
	public static final int LINE_WIDTH=6;
	private Point2D.Float startPos;
	private Point2D.Float endPos;
	private Color trackColour;
	
	// Abstract Methods to be implemented
	public abstract void render(ShapeRenderer renderer);
	public abstract boolean canEnter(boolean forward);
	public abstract void enter(Train t);
	public abstract String toString();
	public abstract void leave(Train t);
	
	/**
	 * Constructor for Track Class.
	 * @param start starting position of Track as a (x,y) coordinate.
	 * @param end ending position of Track as a (x,y) coordinate.
	 * @param trackCol colour of Track.
	 */
	public Track(Point2D.Float start, Point2D.Float end, Color trackCol){
		this.startPos = start;
		this.endPos = end;
		this.trackColour = trackCol;
	}
	
	/**
	 * Getter for starting position.
	 * @return startPos
	 */
	public Point2D.Float getStartPos() {
		return startPos;
	}

	/**
	 * Getter for ending position
	 * @return endPos
	 */
	public Point2D.Float getEndPos() {
		return endPos;
	}
	
	/**
	 * Getter for track colour.
	 * @return trackColour
	 */
	public Color getTrackColour() {
		return trackColour;
	}

}
