package com.unimelb.swen30006.metromadness.tracks;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is a class for DualTrack, which extends the abstract class Track. 
 * This class represents Tracks that can go in two directions.
 */
public class DualTrack extends Track {

    // Checks for which direction is occupied by a Train.
	public boolean forwardOccupied;
	public boolean backwardOccupied;
	
	/**
     * Constructor for DualTrack Class.
     * @param start starting position of Track as a (x,y) coordinate.
     * @param end ending position of Track as a (x,y) coordinate.
     * @param trackCol colour of Track.
     */
	public DualTrack(Float start, Float end, Color col) {
		super(start, end, col);
		this.forwardOccupied = false;
		this.backwardOccupied = false;
	}
	
	/**
     * Renders the Track in the visualisation constantly.
     * @param renderer
     */
	public void render(ShapeRenderer renderer){
		Point2D.Float startPos = getStartPos();
		Point2D.Float endPos = getEndPos();
		
		renderer.rectLine(startPos.x, startPos.y, endPos.x, endPos.y, LINE_WIDTH);
		renderer.setColor(new Color(245f/255f,245f/255f,245f/255f,0.5f).lerp(getTrackColour(), 0.5f));
		renderer.rectLine(startPos.x, startPos.y, endPos.x, endPos.y, LINE_WIDTH/3);
		renderer.setColor(getTrackColour());
	}
	
	/**
     * Sets appropriate occupied variable to true as a result of Train entering Track.
     * @param t Train entering Track.
     */
	@Override
	public void enter(Train t){
		if(t.isForward()){
			this.forwardOccupied = true;
		} else {
			this.backwardOccupied = true;
		}
	}

    /**
     * Checks if the Track is empty and can be entered.
     * @param forward direction of Train
     */
	@Override
	public boolean canEnter(boolean forward) {
		if(forward){
			return !this.forwardOccupied;
		} else {
			return !this.backwardOccupied;
		}
	}

	/**
     * Sets appropriate occupied variable to false as a result of Train leaving Track.
     * @param t Train leaving Track.
     */
	@Override
	public void leave(Train t) {
		if(t.isForward()){
			this.forwardOccupied = false;
		} else {
			this.backwardOccupied = false;
		}
	}

    /**
     * Returns a string representation of this object.
     * @return String
     */
	@Override
	public String toString() {
		return "Track [startPos=" + getStartPos() + ", endPos=" + getEndPos() + ", trackColour=" + getTrackColour() + ", forwardOccupied="
				+ forwardOccupied + ", backwardOccupied=" + backwardOccupied + "]";
	}
}
