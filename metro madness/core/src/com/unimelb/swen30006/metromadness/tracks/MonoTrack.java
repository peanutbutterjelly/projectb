package com.unimelb.swen30006.metromadness.tracks;

import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is a class for MonoTrack, which extends the abstract class Track. 
 * This class represents Tracks that only go in one direction.
 */
public class MonoTrack extends Track {

    // Check if the Track is currently occupied by a Train.
	public boolean occupied;
	
	/**
     * Constructor for MonoTrack Class.
     * @param start starting position of Track as a (x,y) coordinate.
     * @param end ending position of Track as a (x,y) coordinate.
     * @param trackCol colour of Track.
     */
	public MonoTrack(Float start, Float end, Color trackCol) {
		super(start, end, trackCol);
		this.occupied = false;
	}

	/**
     * Renders the Track in the visualisation constantly.
     * @param renderer the gdx ShapeRenderer
     */
	public void render(ShapeRenderer renderer){
		Point2D.Float startPos = getStartPos();
		Point2D.Float endPos = getEndPos();
		
		renderer.rectLine(startPos.x, startPos.y, endPos.x, endPos.y, LINE_WIDTH);
	}
	
    /**
     * Checks if the Track is empty and can be entered.
     * @param forward direction of Train
     */
	@Override
	public boolean canEnter(boolean forward){
		return !this.occupied;
	}

    /**
     * Sets occupied variable to true due to Train entering Track.
     */
	@Override
	public void enter(Train t){
		this.occupied = true;
	}

    /**
     * Returns a string representation of this object.
     * @return String
     */
	@Override
	public String toString() {
		return "Track [startPos=" + getStartPos() + ", endPos=" + getEndPos() + ", trackColour=" + getTrackColour() + ", occupied="
				+ occupied + "]";
	}

    /**
     * Sets occupied variable to false due to Train leaving Track.
     */
	public void leave(Train t){
		this.occupied = false;
	}
}
