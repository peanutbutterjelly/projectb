package com.unimelb.swen30006.metromadness.passengers;

import java.util.Random;

import com.unimelb.swen30006.metromadness.routers.SimpleRouter;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.trains.Train;

public class SimplePassenger extends Passenger {
	
	/**
	 * Constructor for SimplePassenger class
	 * @param id The id of the passenger
	 * @param random a random generator
	 * @param start The start station
	 * @param end The end station
	 * @param cargo The maximum cargo that the passenger can carry
	 */
	public SimplePassenger(int id, Random random, Station start, Station end, int cargo){
		super(id, random, start, end, cargo);
		super.setRouter(SimpleRouter.getInstance());
	}
	
	@Override
	public boolean shouldLeave(){
		Train ridingOn = this.getRidingOn();
		return this.getRouter().shouldLeave(ridingOn.currentStation(), this);
	}
	
	@Override
	public boolean shouldBoard(Train t){
		return this.getRouter().shouldBoard(t, this);
		//return t.getTrainLine().getStations().contains(this.getDestination());
	}
}
