package com.unimelb.swen30006.metromadness.passengers;

import java.util.Random;

import com.unimelb.swen30006.metromadness.routers.PassengerRouter;
import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.trains.Train;

/**
 * This is an abstract class for all Stations.
 */
public abstract class Passenger {
	// attributes
	private final  int id;
	private Station beginning;
	private Station destination;
	private float travelTime;
	private int cargoWeight;

	private Train ridingOn;
	private PassengerRouter router;
	
	/**
	 * Constructor for Passenger class
	 * @param id The id of the passenger
	 * @param random a random generator
	 * @param start The start station
	 * @param end The end station
	 * @param cargo The maximum cargo that the passenger can carry
	 */
	public Passenger(int id, Random random, Station start, Station end, int cargo){
		this.id = id;
		this.beginning = start;
		this.destination = end;
		this.travelTime = 0;
		if(cargo == 0){
			this.cargoWeight = 0;
		}else{
			this.cargoWeight = generateCargo(random, cargo);
		}
	}
	
	/**
	 * Getter for the beginning station
	 * @return Beginning station
	 */
	public Station getBeginning() {
		return beginning;
	}

	/**
	 * Getter for the destination station
	 * @return Destination station
	 */
	public Station getDestination() {
		return destination;
	}

	/**
	 * Getter for the travel time
	 * @return Travel time
	 */
	public float getTravelTime() {
		return travelTime;
	}

	/**
	 * Passenger has reach destination
	 * @return true if the passenger reaches the destination
	 */
	public boolean isReachedDestination() {
		return this.ridingOn.currentStation() == this.destination;
	}

	/**
	 * Getter for the passenger id
	 * @return The passenger id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Getter for the train that the passenger is riding on
	 * @return The train that the passenger is riding on
	 */
	public Train getRidingOn() {
		return ridingOn;
	}

	/**
	 * Setter for the train that the passenger is riding on
	 * @param ridingOn The train that the passenger is riding on
	 */
	public void setRidingOn(Train ridingOn) {
		this.ridingOn = ridingOn;
	}

	/**
	 * Getter for the router that the passenger is using
	 * @return The router that the passenger is using
	 */
	public PassengerRouter getRouter() {
		return router;
	}

	/**
	 * Setter for the router that the passenger will be using
	 * @param router The router that the passenger will be using
	 */
	public void setRouter(PassengerRouter router) {
		this.router = router;
	}

	/**
	 * Update the passenger
	 * @param time the time difference from last update
	 */
	public void update(float time){
		if(!this.isReachedDestination()){
			this.travelTime += time;
		}
	}

	/**
	 * Getter the cargo weight that the passenger is carrying
	 * @return the cargo weight that the passenger is carrying
	 */
	public int getCargoWeight(){
		return this.cargoWeight;
	}
	
	/**
	 * Generate cargo for passenger
	 * @param random The random generator
	 * @param cargo the maximum weight of the cargo
	 * @return the weight generated
	 */
	public int generateCargo(Random random, int cargo){
		return cargoWeight = random.nextInt(cargo);
	}

	/**
	 * Should the passenger leave
	 * @return true if the passenger should leave
	 */
	public abstract boolean shouldLeave();
	
	/**
	 * Should the passenger board a train
	 * @param t The train being considered by the passenger
	 * @return true if the passenger should board this train
	 */
	public abstract boolean shouldBoard(Train t);
}
