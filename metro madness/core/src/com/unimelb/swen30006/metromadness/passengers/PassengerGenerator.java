package com.unimelb.swen30006.metromadness.passengers;

import java.util.ArrayList;
import java.util.Random;

import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.tracks.Line;

/**
 * This is an abstract class for all PassengerGenerator.
 * This is a generator for generating passengers
 */
public class PassengerGenerator {
	
	// Random number generator
	private static final  Random random = new Random(30006);
	
	// Passenger id generator
	private static  int idGen = 1;
	
	// The station that passengers are getting on
	private Station s;
	// The line they are travelling on
	private ArrayList<Line> lines;
	
	// The max volume
	private int maxVolume;
	
	/**
	 * Constructor for PassengerGenerator class
	 * @param s The station that this generator is associated to
	 * @param lines The lines that this generator should generate
	 * @param max Maximum volume of the generator will generate at once
	 */
	public PassengerGenerator(Station s, ArrayList<Line> lines, int max){
		this.s = s;
		this.lines = lines;
		this.maxVolume = max;
	}
	
	/**
	 * Standard way of getting id when generators are generating passenger
	 * @return The id the new passenger should take
	 */
	public static int getIdGen(){
		return PassengerGenerator.idGen++;
	}
	
	/**
	 * Getter for the max volume
	 * @return The max volume
	 */
	public int getMaxVolume(){
		return this.maxVolume;
	}
	
	/**
	 * Getter for the random
	 * @return The random object
	 */
	public static Random getRandom(){
		return PassengerGenerator.random;
	}
	
	/**
	 * Getter for the station associated with
	 * @return The station associated with
	 */
	public Station getStation(){
		return this.s;
	}
	
	/**
	 * Getter for the lines that this generator will generate passenger to
	 * @return The lines that this generator will generate passenger to
	 */
	public ArrayList<Line> getLines(){
		return this.lines;
	}
	
	/**
	 * Generate passenger up to max
	 * @return The passengers generated
	 */
	public Passenger[] generatePassengers(){
		int count = random.nextInt(this.getMaxVolume())+1;
		Passenger[] passengers = new Passenger[count];
		
		ArrayList<Station> stations = new ArrayList<Station>();
		for(Line line : lines){
			stations.addAll(line.getStations());
		}
		
		for(int i=0; i<count; i++){
			passengers[i] = generatePassenger(random, stations, 0);
		}
		return passengers;
	}
	
	/**
	 * Generate one single passenger
	 * @param random The random object
	 * @param stations The stations that the passenger can go
	 * @param maxCargo The maximum cargo that the passenger can carry
	 * @return The passenger generated
	 */
	public Passenger generatePassenger(Random random, 
			ArrayList<Station> stations, int maxCargo){
		// Pick a random station from stations
		Station s = stations.get(random.nextInt(stations.size()));
		
		return new SimplePassenger(idGen++, random, this.s, s, maxCargo);
	}
	
}
