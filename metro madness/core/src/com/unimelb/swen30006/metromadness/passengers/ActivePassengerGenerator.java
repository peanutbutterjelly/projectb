package com.unimelb.swen30006.metromadness.passengers;

import java.util.ArrayList;
import java.util.HashSet;

import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.tracks.Line;

public class ActivePassengerGenerator extends PassengerGenerator {
	
	private ArrayList<Station> possibleStations;
	private boolean generated = false;
	
	/**
	 * Constructor for ActivePassengerGenerator class
	 * @param s The station that this generator is associated to
	 * @param lines The lines that this generator should generate
	 * @param max Maximum volume of the generator will generate at once
	 */
	public ActivePassengerGenerator(Station s, ArrayList<Line> lines, int max){
		super(s, lines, max);
		this.possibleStations = new ArrayList<Station>();
	}
	
	@Override
	public Passenger[] generatePassengers(){
		int count = PassengerGenerator.getRandom().nextInt(4)+1;
		Passenger[] passengers = new Passenger[count];
		
		if(!generated){
			HashSet<Station> stations = new HashSet<Station>();
			for(Line line : this.getLines()){
				for(Station station : line.getStations()){
					stations.add(station);
				}
			}
			for(Station station : stations){
				possibleStations.add(station);
			}
			generated = true;
		}
		
		for(int i=0; i<count; i++){
			passengers[i] = generatePassenger(PassengerGenerator.getRandom(), possibleStations, 0);
		}
		return passengers;
	}
	
}
