package com.unimelb.swen30006.metromadness.passengers;

import java.util.ArrayList;
import java.util.HashSet;

import com.unimelb.swen30006.metromadness.stations.Station;
import com.unimelb.swen30006.metromadness.stations.CargoStation;
import com.unimelb.swen30006.metromadness.tracks.Line;

public class CargoPassengerGenerator extends PassengerGenerator {

	// Max Cargo per passenger
	private static final int MAX_CARGO = 51;
	
	private ArrayList<Station> possibleStations;
	private boolean generated = false;
	
	/**
	 * Constructor for CargoPassengerGenerator class
	 * @param s The station that this generator is associated to
	 * @param lines The lines that this generator should generate
	 * @param max Maximum volume of the generator will generate at once
	 */
	public CargoPassengerGenerator(Station s, ArrayList<Line> lines, int max){
		super(s, lines, max);
		this.possibleStations = new ArrayList<Station>();
	}
	
	@Override
	public Passenger[] generatePassengers(){
		int count = PassengerGenerator.getRandom().nextInt(4)+1;
		Passenger[] passengers = new Passenger[count];
		
		if(!generated){
			HashSet<Station> stations = new HashSet<Station>();
			for(Line line : this.getLines()){
				for(Station station : line.getStations()){
					if(station instanceof CargoStation){
						stations.add(station);
					}
				}
			}
			for(Station station : stations){
				possibleStations.add(station);
			}
			generated = true;
		}
		
		for(int i=0; i<count; i++){
			passengers[i] = generatePassenger(PassengerGenerator.getRandom(), possibleStations, MAX_CARGO);
		}
		return passengers;
	}

}
